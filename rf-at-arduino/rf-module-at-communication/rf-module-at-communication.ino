/*
 * This Arduino can send serial to a device connected to the software serial pins. This
 * was created to enable communication with a serial device through the serial monitor.
 */

#include <SoftwareSerial.h>

SoftwareSerial ExternalDevice(10, 11); // RX, TX. This is where the RF module is connected to.

void setup() {
  // Initialise the serial to the computer.
  Serial.begin(9600);
  Serial.setTimeout(500);
  // Initialise the RS485 serial.
  ExternalDevice.begin(9600);
  ExternalDevice.setTimeout(500);
}

void loop() {
  // Check for available serial from computer.
  if (Serial.available()) {
    ExternalDevice.print((char)Serial.read());
  }
  // Check for available serial from remote.
  if (ExternalDevice.available() > 0) {
    Serial.print((char)ExternalDevice.read());
  }
}
