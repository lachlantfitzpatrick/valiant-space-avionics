#include "CommsConfig.h"

namespace commsconfig {

// Error code names.
    const char *ErrorCode[commsconfig::END_ERROR_CONFIG] = {
            "NO_ERROR",
            "UNRECOGNISED_PACKET",
            "INCORRECT_NUMBER_ARGS",
            "NO_ARGS_RECEIVED",
            "INCORRECT_ADDRESS_FORMAT",
            "INCORRECT_PARAMETER_FORMAT",
            "ADDRESS_OUT_OF_RANGE",
            "INVALID_DATA_TO_WRITE",
            "INCORRECT_DATA_DESIGNATOR",
            "TIMEOUT",
            "WRITE_NOT_ALLOWED",
            "FAILED_TO_SEND",
            "FAILED_TO_OPEN",
            "FAILED_TO_CONNECT",
            "NO_CONNECTION",
            "INCORRECT_RESPONSE_FORMAT",
            "INCORRECT_DATA_TYPE_RESPONSE",
            "INCORRECT_DATA_VALUE_RESPONSE",
            "COMMS_BUSY"
    };

}

namespace commsdata {

    // Datastructures to store comms state in.
    bool coils[commsconfig::END_COILS] = {};
    int16_t input_regs[commsconfig::END_INPUT_REGS] = {};
    int16_t holding_regs[commsconfig::END_HOLDING_REGS] = {};

}
