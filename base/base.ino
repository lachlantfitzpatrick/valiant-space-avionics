/*
 * This Arduino acts as a pipe between the remote arduino and the
 * computer. It takes commands from the computer, converts them
 * into Modbus messages, sends them to the remote arduino,
 * listen to messages back from the remote arduino and passes 
 * these messages back to the computer.
 */

#include <SoftwareSerial.h>

#include "CommsConfig.h"

using namespace commsconfig;

SoftwareSerial rs485(10, 11); // RX, TX

void setup() {
  // Initialise the serial to the computer.
  Serial.begin(kBaud);
  Serial.setTimeout(kTimeout);
  // Initialise the RS485 serial.
  rs485.begin(kBaud);
  rs485.setTimeout(kTimeout);
}

// Strip the command out of a packet. Assumes it has already been checked.
String strip_packet(String packet) {
  // Remove the preamble and postamble characters in a string.
  return packet.substring(kAmbleLength, packet.length()-kAmbleLength);
}

// Packs a command to pipe.
String pack_packet(String command, Destination destination) {
  // Add the preamble, postamble and newline to command.
  String empty = "";
  if (destination == COMPUTER) return kPreambleComp + command + kPostambleComp + '\n';
  else if (destination == REMOTE) return kPreambleRem + command + kPostambleRem + '\n';
  else return empty + kPreambleComp + kError + kDelim + UNRECOGNISED_PACKET + kPostambleComp + '\n';
}

// Pipe the packet to the other device if it is in the right format.
void pipe_packet(String packet) {
  // If the packet came from the computer, strip it, repack it and send to remote.
  if (packet.startsWith(String(kPreambleRem)) && packet.endsWith(String(kPostambleRem))) {
    rs485.print(packet + '\n');
  }
  // If the packet came from the remote, strip it, repack it and send to computer.
  else if (packet.startsWith(String(kPreambleComp)) && packet.endsWith(String(kPostambleComp))) {
    Serial.print(packet + '\n');
  }
  // If the packet wasn't in a recognised format send an error to computer.
  else {
    String empty = "";
    Serial.print(empty + kPreambleComp + kError + kDelim + UNRECOGNISED_PACKET + kPostambleComp + '\n');
  }
}

void loop() {
  // Check for available serial from computer.
  if (Serial.available()) {
    // Read the serial to extract the command. 
    // IMPORTANT. Packets can't be longer than 64bytes as this is the buffer length.
    pipe_packet(Serial.readStringUntil('\n'));
  }
  // Check for available serial from remote.
  if (rs485.available() > 0) {
    // Read the serial to extract the command. 
    // IMPORTANT. Packets can't be longer than 64bytes as this is the buffer length.
    pipe_packet(rs485.readStringUntil('\n'));
  }
}
