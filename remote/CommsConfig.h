// This is the configuration file for a custom communications protocol designed for two devices. 
// It utilises modbus style data storage and access without the memory consumption of a full 
// modbus implementation with an additional feature for streaming input registers.

#ifndef COMMS_CONFIG_H
#define COMMS_CONFIG_H

// Include non standard int types.
#include <stdint.h>

// Namespace defining the configuration.
namespace commsconfig {

    // Period in milliseconds to wait on a serial message before timing out.
    const long kTimeout = 1000;
    // The baud rate to use.
    const long kBaud = 57600;

    // Destionation packing configuration.
    const int kAmbleLength = 4;
    const char kPreambleComp[kAmbleLength+1] = "H7#d";
    const char kPostambleComp[kAmbleLength+1] = "2!oF";
    const char kPreambleRem[kAmbleLength+1] = "F&s0";
    const char kPostambleRem[kAmbleLength+1] = "*xI5";

    // Command configuration.
    const char kRead[2] = "r";
    const int kReadArgs = 3;
    const int kReadResponseArgs = 4;
    const int kReadDataStackIndex = 1;
    const int kReadDataAddressIndex = 2;
    const int kReadDataValueIndex = 3;
    const char kWrite[2] = "w";
    const int kWriteArgs = 4;
    const int kWriteResponseArgs = 4;
    const int kWriteDataStackIndex = 1;
    const int kWriteDataAddressIndex = 2;
    const int kWriteDataValueIndex = 3;
    const char kStream[2] = "d";
    // Note the number of stream args is actually variable depending on the number of input regs
    // being streamed however it is guaranteed to be 4 or greater. Eg. "d 5000 10 1" streams 
    // register 1 for 5 seconds at 10 millisecond intervals.
    const int kStreamArgs = 4;
    const int kStreamResponseArgs = 3;
    const int kStreamDurationIndex = 1;
    const int kStreamPeriodIndex = 2;
    const int kStreamRegisterStartIndex = 3;
    const char kError[2] = "e";
    const int kErrorArgs = 2;
    // Designators for the different data stacks.
    const char kCoil[2] = "c";
    const char kInputReg[2] = "i";
    const char kHoldingReg[2] = "h";
    const char kDelim[2] = " ";

    // Communication lines.
    enum Destination {
        COMPUTER,
        REMOTE
    };
    // Coil configuration.
    enum CoilConfig {
        TEST_COIL,
        PRES_SERVO,
        FUEL_FILL_SERVO,
        FUEL_MAIN_SERVO,
        OXI_FILL_SERVO,
        OXI_MAIN_SERVO,
        AUTO_SEQ,
        AUTO_SEQ_DEP,
        IGNITION,
        EMERGENCY_STOP,
        END_COILS
    };
    // Input register configuration.
    enum InputRegConfig {
        PRES_SENS_P,
        FUEL_TANK_SENS_P,
        FUEL_FEED_SENS_P,
        OXI_TANK_PRES_P,
        OXI_FEED_PRES_P,
        END_INPUT_REGS
    };
    // Holding register configuration.
    enum HoldingRegConfig {
        PRES_SERVO_OPEN,
        PRES_SERVO_CLOSE,
        FUEL_FILL_SERVO_OPEN,
        FUEL_FILL_SERVO_CLOSE,
        FUEL_MAIN_SERVO_OPEN,
        FUEL_MAIN_SERVO_CLOSE,
        OXI_FILL_SERVO_OPEN,
        OXI_FILL_SERVO_CLOSE,
        OXI_MAIN_SERVO_OPEN,
        OXI_MAIN_SERVO_CLOSE,
        INPUT_REG_UPDATE_PERIOD,
        END_HOLDING_REGS
    };
    // Error codes.
    enum ErrorConfig {
        NO_ERROR,
        UNRECOGNISED_PACKET,
        INCORRECT_NUMBER_ARGS,
        NO_ARGS_RECEIVED,
        INCORRECT_ADDRESS_FORMAT,
        INCORRECT_PARAMETER_FORMAT,
        ADDRESS_OUT_OF_RANGE,
        INVALID_DATA_TO_WRITE,
        INCORRECT_DATA_DESIGNATOR,
        TIMEOUT,
        WRITE_NOT_ALLOWED, // Not allowed to write to an input register.
        FAILED_TO_SEND,
        FAILED_TO_OPEN,
        FAILED_TO_CONNECT,
        NO_CONNECTION,
        INCORRECT_RESPONSE_FORMAT,
        INCORRECT_DATA_TYPE_RESPONSE,
        INCORRECT_DATA_VALUE_RESPONSE,
        COMMS_BUSY,
        END_ERROR_CONFIG
    };
    // Error code names.
    extern const char* ErrorCode[END_ERROR_CONFIG];

}
// Datastructures to store the data defined in the protocol on the device.
namespace commsdata {

    extern bool coils[commsconfig::END_COILS];
    extern int16_t input_regs[commsconfig::END_INPUT_REGS];
    extern int16_t holding_regs[commsconfig::END_HOLDING_REGS];

}

#endif // COMMS_CONFIG_H
