
/* Char Inputs:
   -> 2 = Helium Servo Close
   -> 3 = Helium Servo Open
   -> 4 = Fuel Fill/Vent Servo Close
   -> 5 = Fuel Fill/Vent Servo Open
   -> 6 = Fuel Main Servo Close
   -> 7 = Fuel Main Servo Open
   -> 8 = Close All
   -> 9 = Open All
   -> 10 = Autosequence Start
   -> 11 = Autosequence Abort
   -> 12 = Autosequence Smart Abort / Control Failure
   -> 13 = Autosequence Continue

   Char Outpus:
   -> 115 = Successfully Connected
   -> 254 = Autosequence Stage 1: Countdown 30 seconds
   -> 253 = Autosequence Stage 2: VFV Close, Range Clear 10 seconds
   -> 252 = Autosequence Stage 3: HEV Open, Wait 10 seconds
   -> 251 = Autosequence Stage 4: Final Countdown 10 seconds
   -> 250 = Autosequence Stage 5: MFV Open
   -> 249 = Autosequence Stage 6: MFV + HEV Close, VFV Open
   ->
*/

#include <Servo.h>
#include <time.h>

#include "CommsConfig.h"

#define LED       13
#define HE_CLOSE  -40
#define HE_OPEN   90
#define FM_CLOSE  120
#define FM_OPEN   28
#define FV_CLOSE  90
#define FV_OPEN   0
#define OM_CLOSE  0 // default value, needs measuring
#define OM_OPEN   0 // default value, needs measuring
#define OV_CLOSE  0 // default value, needs measuring
#define OV_OPEN   0 // default value, needs measuring
#define SENS_READ 10 // 10ms per sensor read

#define PRESSURANT_SENSOR   A0
#define FUEL_TANK_SENSOR    A1
#define FUEL_FEED_SENSOR    A2
#define OXI_TANK_SENSOR     A3
#define OXI_FEED_SENSOR     A4

#define IGNITION_PIN        2
#define IGNITION_DURATION   1000 // ms

using namespace commsdata;
using namespace commsconfig;

bool vHeOpen = true;
bool vFuVOpen = true;
bool vFuMOpen = true;

Servo valveHe;
Servo valveFuV;
Servo valveFuM;

bool ledOn = false;
bool firstTime = true;
byte num = 0;

double marker = 0;
bool autoDepSeq = false;
bool autoNonDepSeq = false;
bool dumbAbort = false;
//bool controlFailAbort = false;

unsigned long last_input_register_update = 0;
unsigned long ignition_time = 0;

// Flag to indicate whether we are streaming data.
bool is_streaming = false;
int registers_streaming[END_INPUT_REGS];
int number_registers_streaming;
unsigned long stream_duration;
unsigned long stream_period;
unsigned long start_stream_time = 0;
unsigned long last_stream_time = 0;
// Flag to indicate if we are igniting.
bool ignition = false;

void setup() {
  // Initialise the serial using the baud and timeout from CommsConfig.h
  Serial.begin(kBaud);
  Serial.setTimeout(kTimeout);
  pinMode(LED, OUTPUT); // Sets output to LED
  pinMode(IGNITION_PIN, OUTPUT);
  valveHe.attach(9);  // attaches the servo on pin 9 to the servo object
  valveFuV.attach(10);  // attaches the servo on pin 10 to the servo object
  valveFuM.attach(11);  // attaches the servo on pin 11 to the servo object

  // Setup the holding registers.
  holding_regs[PRES_SERVO_OPEN] = HE_OPEN;
  holding_regs[PRES_SERVO_CLOSE] = HE_CLOSE;
  holding_regs[FUEL_FILL_SERVO_OPEN] = FV_OPEN;
  holding_regs[FUEL_FILL_SERVO_CLOSE] = FV_CLOSE;
  holding_regs[FUEL_MAIN_SERVO_OPEN] = FM_OPEN;
  holding_regs[FUEL_MAIN_SERVO_CLOSE] = FM_CLOSE;
  holding_regs[OXI_FILL_SERVO_OPEN] = OV_OPEN;
  holding_regs[OXI_FILL_SERVO_CLOSE] = OV_CLOSE;
  holding_regs[OXI_MAIN_SERVO_OPEN] = OM_OPEN;
  holding_regs[OXI_MAIN_SERVO_CLOSE] = OM_CLOSE;
  holding_regs[INPUT_REG_UPDATE_PERIOD] = SENS_READ;

  // Close all valves initially.
  FuelMain_close();
  FuelVent_close();
  Helium_close();

}

void sequence(double when, double duration, void (*actions[])(), byte id, int sizeArray) {
  double timeElapsed = millis() - marker;
  if (timeElapsed > 1000 * when && timeElapsed < 1000 * (when + duration)) {
    for (int i = 0; i < sizeArray; i++) {
      actions[i]();
    }
//    Serial.println(id);
  }
}

void Helium_open() {
  coils[PRES_SERVO] = true;
  if (!vHeOpen) {
    valveHe.write(holding_regs[PRES_SERVO_OPEN]);
    vHeOpen = true;
  }
}

void Helium_close() {
  coils[PRES_SERVO] = false;
  if (vHeOpen) {
    vHeOpen = false;
  }
}

void FuelMain_open() {
  coils[FUEL_MAIN_SERVO] = true;
  if (!vFuMOpen) {
    valveFuM.write(holding_regs[FUEL_MAIN_SERVO_OPEN]);
    vFuMOpen = true;
  }
}

void FuelMain_close() {
  coils[FUEL_MAIN_SERVO] = false;
  if (vFuMOpen) {
    valveFuM.write(holding_regs[FUEL_MAIN_SERVO_CLOSE]);
    vFuMOpen = false;
  }
}

void FuelVent_open() {
  coils[FUEL_FILL_SERVO] = true;
  valveFuV.write(holding_regs[FUEL_FILL_SERVO_OPEN]);
}

void FuelVent_close() {
  coils[FUEL_FILL_SERVO] = false;
  valveFuV.write(holding_regs[FUEL_FILL_SERVO_CLOSE]);
}

void update_input_registers() {
  // Set the last read time to now.
  last_input_register_update = millis();
  // Update all the input registers.
  input_regs[PRES_SENS_P] = analogRead(PRESSURANT_SENSOR);
  input_regs[FUEL_TANK_SENS_P] = analogRead(FUEL_TANK_SENSOR);
  input_regs[FUEL_FEED_SENS_P] = analogRead(FUEL_FEED_SENSOR);
  input_regs[OXI_TANK_PRES_P] = analogRead(OXI_TANK_SENSOR);
  input_regs[OXI_FEED_PRES_P] = analogRead(OXI_FEED_SENSOR);
}

void blinking() {
  long freq = 300;
  if (fmod((millis() - marker), 2 * freq) < freq) {
    digitalWrite(LED, HIGH);
  } else {
    digitalWrite(LED, LOW);
  }
}

void blinking_fast() {
  long freq = 50;
  if (fmod((millis() - marker), 2 * freq) < freq) {
    digitalWrite(LED, HIGH);
  } else {
    digitalWrite(LED, LOW);
  }
}

void solid_light() {
  digitalWrite(LED, HIGH);
}

void no_light() {
  digitalWrite(LED, LOW);
}

void ignite() {
  digitalWrite(IGNITION_PIN, HIGH);
  ignition_time = millis();
  ignition = true;
}

// Autosequence stages for Full Depletion Test
void (*autoDepStage1[])() = { blinking_fast };
void (*autoDepStage2[])() = { Helium_open };
void (*autoDepStage3[])() = { blinking_fast };
void (*autoDepStage4[])() = { FuelMain_open, solid_light, ignite };
void (*autoDepStage5[])() = { Helium_close, no_light };

// Autosequence stages for Non-Depletion Test
void (*autoNonDepStage1[])() = { blinking_fast };
void (*autoNonDepStage2[])() = { Helium_open };
void (*autoNonDepStage3[])() = { blinking_fast };
void (*autoNonDepStage4[])() = { FuelMain_open, solid_light, ignite };
void (*autoNonDepStage5[])() = { FuelMain_close, Helium_close, no_light };

// Check whether two c strings are equal.
bool string_equals(const char *str1, const char *str2) {
  return strcmp(str1, str2) == 0;
}

// Gets the length of the command string if the packet_string is stripped.
uint8_t command_string_length(String packet_string) {
  return packet_string.length() >= 2 * kAmbleLength ?
         packet_string.length() + 1 - 2 * kAmbleLength : 0;
}

// If the packet contained the correct amble it strips it and puts it in
// the command buffer otherwise it returns false.
bool strip_packet(String packet, char command[]) {
  // Check the preamble is correct.
  if (packet.startsWith(kPreambleRem) && packet.endsWith(kPostambleRem)) {
    // Strip the preamble and copy the stripped string to the command buffer.
    packet.substring(kAmbleLength, packet.length() - kAmbleLength).toCharArray(
      command, command_string_length(packet));
    return true;
  }
  else return false;
}

// Packs and sends a message.
void pack_and_send_message(String message) {
  Serial.print(kPreambleComp + message + kPostambleComp + '\n');
}

// Sends an error packet.
void send_error(ErrorConfig error) {
  pack_and_send_message(String(kError) + kDelim + error);
}

// Returns the command string reconstructed from the arguments.
String reconstruct_command(const char *arguments[], uint8_t num_arguments) {
  if (num_arguments == 0) return String("");
  String command = arguments[0];
  for (uint8_t i = 1; i < num_arguments; i++) {
    command = command + kDelim + arguments[i];
  }
  return command;
}

// Process and handle the arguments of a read command.
void handle_read(const char *arguments[], uint8_t num_arguments) {
  // Check that the number of arguments is correct.
  if (num_arguments != kReadArgs) {
    send_error(INCORRECT_NUMBER_ARGS);
    return;
  }
  // Try and get the register or coil number we're reading.
  char *end_ptr;
  int data_pointer = strtol(arguments[kReadDataAddressIndex], &end_ptr, 10);
  if (*end_ptr != '\0') {
    send_error(INCORRECT_ADDRESS_FORMAT);
    return;
  }
  // Read data from the designated data stack.
  if (string_equals(arguments[kReadDataStackIndex], kCoil)) {
    // Check the pointer is in the correct range.
    if (data_pointer >= END_COILS or data_pointer < 0) {
      send_error(ADDRESS_OUT_OF_RANGE);
      return;
    }
    // Pack and send a reply.
    pack_and_send_message(reconstruct_command(arguments, num_arguments) + kDelim + String(coils[data_pointer]));
    return;
  }
  else if (string_equals(arguments[kReadDataStackIndex], kInputReg)) {
    // Check the pointer is in the correct range.
    if (data_pointer >= END_INPUT_REGS or data_pointer < 0) {
      send_error(ADDRESS_OUT_OF_RANGE);
      return;
    }
    // Pack and send a reply.
    pack_and_send_message(reconstruct_command(arguments, num_arguments) + kDelim + String(input_regs[data_pointer]));
    return;
  }
  else if (string_equals(arguments[kReadDataStackIndex], kHoldingReg)) {
    // Check the pointer is in the correct range.
    if (data_pointer >= END_HOLDING_REGS or data_pointer < 0) {
      send_error(ADDRESS_OUT_OF_RANGE);
      return;
    }
    // Pack and send a reply.
    pack_and_send_message(reconstruct_command(arguments, num_arguments) + kDelim + String(holding_regs[data_pointer]));
    return;
  }
  else {
    send_error(INCORRECT_DATA_DESIGNATOR);
    return;
  }
}

// Performs actions based on the coil written.
bool handle_coil_write(CoilConfig coil, int data) {
  switch(coil) {
    case TEST_COIL:
      return true;
    case PRES_SERVO:
      if (data == 1) Helium_open();
      if (data == 0) Helium_close();
      return true;
    case FUEL_FILL_SERVO:
      if (data == 0) FuelVent_close();
      if (data == 1) FuelVent_open();
      return true;
    case FUEL_MAIN_SERVO:
      if (data == 0) FuelMain_close();
      if (data == 1) FuelMain_open();
      return true;
    case OXI_FILL_SERVO:
      break;
    case OXI_MAIN_SERVO:
      break;
    case AUTO_SEQ:
      // Start auto sequence.
      if (data == 1) {
        autoDepSeq = true;
        autoNonDepSeq = false;
        marker = millis();
      }
      // Stop auto sequence.
      else if (data == 0) {
        autoDepSeq = false;
        autoNonDepSeq = false;
        dumbAbort = true;
        Helium_close();
        FuelVent_open();
        FuelMain_close();
        digitalWrite(LED, LOW);
      }
      return true;
    case AUTO_SEQ_DEP:
      // Start depletion auto sequence.
      if (data == 1) {
        autoDepSeq = false;
        autoNonDepSeq = true;
        marker = millis();     
      }
      // Stop depletion auto sequence.
      else if (data == 0) {
        autoDepSeq = false;
        autoNonDepSeq = false;
        dumbAbort = true;
        Helium_close();
        FuelVent_open();
        FuelMain_close();
        digitalWrite(LED, LOW);
      }
      return true;
    case IGNITION:
      if (data == 1) {
        ignite();  
      }
      else {
        digitalWrite(IGNITION_PIN, LOW);
        digitalWrite(LED, LOW);
      }
      return true;
    case END_COILS:
      send_error(ADDRESS_OUT_OF_RANGE);
      break;
  }
  return false;
}

// Process and handle the arguments of a read command.
void handle_write(const char *arguments[], uint8_t num_arguments) {
  // Check that the number of arguments is correct.
  if (num_arguments != kWriteArgs) {
    send_error(INCORRECT_NUMBER_ARGS);
    return;
  }
  // Try and get the register or coil number we're writing.
  char *end_ptr;
  int data_pointer = strtol(arguments[kWriteDataAddressIndex], &end_ptr, 10);
  // Reply with an error if it wasn't an int
  if (*end_ptr != '\0') {
    send_error(INCORRECT_ADDRESS_FORMAT);
    return;
  }
  // Try and get the data that we want to write in.
  int data = strtol(arguments[kWriteDataValueIndex], &end_ptr, 10);
  // Reply with an error if it wasn't an int
  if (*end_ptr != '\0') {
    send_error(INCORRECT_ADDRESS_FORMAT);
    return;
  }
  // Write to the designated data stack.
  if (string_equals(arguments[kWriteDataStackIndex], kCoil)) {
    // Check the pointer is in the correct range.
    if (data_pointer >= END_COILS) {
      send_error(ADDRESS_OUT_OF_RANGE);
      return;
    }
    // Check the data is in the correct range.
    if (data != 1 && data != 0) {
      send_error(INVALID_DATA_TO_WRITE);
      return;
    }
    // Set the coil.
    coils[data_pointer] = data;
    // Perform the action associated with writing the coil.
    if (handle_coil_write((CoilConfig)data_pointer, data)) {
      // Reply with the same command to indicate success.
      pack_and_send_message(reconstruct_command(arguments, num_arguments));
    }
    else {
      send_error(INVALID_DATA_TO_WRITE);      
    }
    return;
  }
  if (string_equals(arguments[kWriteDataStackIndex], kInputReg)) {
    // Send error, input registers can't be externally written.
    send_error(WRITE_NOT_ALLOWED);
    return;
  }
  if (string_equals(arguments[kWriteDataStackIndex], kHoldingReg)) {
    // Check the pointer is in the correct range.
    if (data_pointer >= END_HOLDING_REGS) {
      send_error(ADDRESS_OUT_OF_RANGE);
      return;
    }
    // Set the holding register.
    holding_regs[data_pointer] = data;
    // Perform the action associated with writing the register.
    // todo currently there aren't any actions associated with writing these
    // Reply with the same command to indicate success.
    pack_and_send_message(reconstruct_command(arguments, num_arguments));
    return;
  }
  else {
    send_error(INCORRECT_DATA_DESIGNATOR);
    return;
  }
}

// Stream a packet of data.
void stream_data() {
  // Set the time.
  last_stream_time = millis();
  // Update all the input registers.
  update_input_registers();
  // Create a string to contain the data.
  String data_string(last_stream_time - start_stream_time);
  data_string = data_string + kDelim;
  for (int i = 0; i < number_registers_streaming; i++) {
    // Add the data to the data string.
    data_string = data_string + input_regs[registers_streaming[i]];
    if (i < number_registers_streaming-1) data_string = data_string + kDelim;
  }
  // Send the data.
  pack_and_send_message(data_string);
}

// Process and handle the arguments of a stream command.
void handle_stream(const char* arguments[], uint8_t num_arguments) {
  // Check that the number of arguments is correct.
  if (num_arguments < kStreamArgs) {
    send_error(INCORRECT_NUMBER_ARGS);
    return;
  }
  // Try and get the duration.
  char *end_ptr;
  stream_duration = strtol(arguments[kStreamDurationIndex], &end_ptr, 10);
  // Reply with an error if it wasn't an int
  if (*end_ptr != '\0') {
    send_error(INCORRECT_PARAMETER_FORMAT);
    return;
  }
  // Try and get the period.
  stream_period = strtol(arguments[kStreamPeriodIndex], &end_ptr, 10);
  // Reply with an error if it wasn't an int
  if (*end_ptr != '\0') {
    send_error(INCORRECT_PARAMETER_FORMAT);
    return;
  }
  // Get all the registers.
  number_registers_streaming = num_arguments - kStreamRegisterStartIndex;
  registers_streaming[number_registers_streaming] = {0};
  for (int i = kStreamRegisterStartIndex; i < num_arguments; i++) {
    // Try and get the registers we're reading from.
    int reg = strtol(arguments[i], &end_ptr, 10);
    // Reply with an error if it wasn't an int
    if (*end_ptr != '\0') {
      send_error(INCORRECT_ADDRESS_FORMAT);
      return;
    }
    // Check whether the register is in range.
    if (reg < 0 or reg >= END_INPUT_REGS) {
      send_error(ADDRESS_OUT_OF_RANGE);
      return;
    }
    // Add the register.
    registers_streaming[i - kStreamRegisterStartIndex] = reg;
  }
  // Reply with the same command to indicate success.
  pack_and_send_message(reconstruct_command(arguments, num_arguments));
  // Initialise the stream.
  start_stream_time = millis();
  is_streaming = true;
  // Stream the first line of data.
  stream_data();
}

// Scans a potential command to determine how many arguments it contains.
uint8_t count_arguments(const char *command) {
  uint8_t num_args = 1; // start on 1 as there will be at least 1 argument.
  const char *tmp = command;
  while ((tmp = strstr(tmp, kDelim))) {
    tmp++;
    num_args++;
  }
  return num_args;
}

// Processes a packet including stripping and splitting the packet, and then
// executing the command.
void handle_packet(String packet) {
  // Buffer to use to process the packet.
  char command[command_string_length(packet)];
  // Check whether we can successfully strip a command from the packet.
  if (strip_packet(packet, command)) {
    // Start processing the command to extract the arguments.
    const char *arguments[count_arguments(command)]; // A pointer to an array of char pointers
    char *token = strtok(command, kDelim);
    // Send an error if there is nothing to process.
    if (token == NULL) {
      send_error(NO_ARGS_RECEIVED);
      return;
    }
    // Iterate and store the other tokens.
    uint8_t i = 0;
    while (token != NULL) {
      arguments[i] = token;
      i++;
      token = strtok(NULL, kDelim);
    }

    // Handle the arguments.
    if (string_equals(arguments[0], kRead)) handle_read(arguments, i);
    if (string_equals(arguments[0], kWrite)) handle_write(arguments, i);
    if (string_equals(arguments[0], kStream)) handle_stream(arguments, i);
  }
}

void loop() {
  // Serial Reading
  if (Serial.available() > 0) {
    handle_packet(Serial.readStringUntil('\n'));
  }

  // Auto Sequence
  if (autoDepSeq) {
    // t = time elapsed (seconds)
    // dur = duration (seconds)
    // stage = current stage
    // out = output number to verify for
    //      (t, dur, stage,  out)
    sequence(0,  20, autoDepStage1, 255, 1);  // 10 second countdown
    sequence(20, 30, autoDepStage2, 254, 1);  // Open Helium Valve, WAIT for stable pressure
    sequence(50, 20, autoDepStage3, 253, 1);  // 20 second countdown till fire
    sequence(70,  5, autoDepStage4, 252, 3);  // Fire!
    sequence(75, 10, autoDepStage5, 251, 2);  // 
  }

  if (autoNonDepSeq) {
    // t = time elapsed (seconds)
    // dur = duration (seconds)
    // stage = current stage
    // out = output number to verify for
    //      (t, dur, stage,  out)
    sequence(0,  20, autoNonDepStage1, 255, 1);  // 10 second countdown
    sequence(20, 30, autoNonDepStage2, 254, 1);  // Open Helium Valve, WAIT for stable pressure
    sequence(50, 20, autoNonDepStage3, 253, 1);  // 20 second countdown till fire
    sequence(70,  3, autoNonDepStage4, 252, 3);  // Fire!
    sequence(75, 10, autoNonDepStage5, 251, 3);  // 
  }

  // Perform timing operations.
  unsigned long current_time = millis();
  // Check for streaming.
  if (is_streaming and current_time - last_stream_time > stream_period) {
    // Stream the data.
    stream_data();
    // Check whether we have exceeded the duration.
    if (current_time - start_stream_time > stream_duration) {
      // Send a message to indicate streaming is over.
      pack_and_send_message(String(kStream) + kDelim + stream_duration + kDelim + stream_period);
      // Set the stream flag to off.
      is_streaming = false;
    }
  }
  // Update the input register data.
  if (current_time - last_input_register_update > (unsigned long)holding_regs[INPUT_REG_UPDATE_PERIOD]) {
    // Update all the input registers.
    update_input_registers();
  }
  // Check for turn off of the ignition system.
  if (ignition and current_time - ignition_time > IGNITION_DURATION) {
    digitalWrite(IGNITION_PIN, LOW);
    ignition = false;
  }
}
