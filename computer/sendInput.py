import sys
import serial

port = sys.argv[1]
inp = sys.argv[2]


byteArray = bytearray(inp.encode('ascii'))

ser = serial.Serial(port)  # open serial port
ser.write(byteArray[0])    # write a char
ser.close()                # close port