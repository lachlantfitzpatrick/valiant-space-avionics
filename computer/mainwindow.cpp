#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QTime>
#include <QTimer>


MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent), ui(new Ui::MainWindow), watch(new Stopwatch()), comms("./") {
    ui->setupUi(this);

    this->setFixedSize(this->size());

    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
        ui->portSelectBox->addItem(info.portName());

    //signal
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(10);

    ui->consoleOutput->ensureCursorVisible();
    ui->consoleOutput->setCenterOnScroll(true);

}

MainWindow::~MainWindow() {
    delete ui;
    delete watch;
}

void MainWindow::update()
{
    if(watch->isRunning())
    {
        qint64 time = watch->getTime();
        int s = int(time / 1000) - 70;
        if (s < 0) {
            ui->LCDWatch->display(QString("T-%1").arg(s*-1));
        } else {
            ui->LCDWatch->display(QString("T+%1").arg(s));
        }
        numOuts++;
        u_char pressureSensorOut = 0;
    }
}

void MainWindow::on_connectBtn_clicked() {
    auto result = comms.Connect("/dev/" + ui->portSelectBox->currentText().toStdString());
    if (result == commsconfig::NO_ERROR) {
        printConsole(QString::fromStdString(std::string("Connected.")));
        isConnected = true;
    }
    else {
        printConsole(tr("Failed connection."));
    }
}

void MainWindow::on_disconnectBtn_clicked() {
    comms.Disconnect();
    printConsole(QString::fromStdString(std::string("Disconnected.")));
}

void MainWindow::on_testBtn_clicked() {
    printConsole(QString::fromStdString(std::string(comms.TestConnection(2000))));
//    printConsole(QString::fromStdString(std::to_string(
//            comms.ReadHoldingRegister(commsconfig::PRES_SERVO_CLOSE).value)));
//    printConsole(QString::fromStdString(std::to_string(
//            comms.WriteHoldingRegister(commsconfig::PRES_SERVO_CLOSE, -100).value)));
//    printConsole(QString::fromStdString(std::to_string(
//            comms.ReadHoldingRegister(commsconfig::PRES_SERVO_CLOSE).value)));
//    printConsole(QString::fromStdString(std::to_string(
//            comms.ReadInputRegister(commsconfig::PRES_SENS_P).value)));
    comms.StreamInputRegisters("./stream.data",
            {commsconfig::PRES_SENS_P, commsconfig::FUEL_FEED_SENS_P}, 5000, 10);
//    comms.WriteCoil(commsconfig::IGNITION, true);
}

void MainWindow::printResponse(const QString &s) {
    if (s == "1" && checkingConnection) { // successful connection return
        connectSuccess = true;
        printConsole(s);
    } else if (s != "1" && checkingConnection) {
        connectSuccess = false;
        printConsole(tr("Could not connect! Returned value was: %1").arg(s));
    } else {
        printConsole(s);
    }
}

void MainWindow::processError(const QString &s) {
    printConsole(tr("Error: %1").arg(s));
}

void MainWindow::processTimeout(const QString &s) {
    printConsole(tr("Timeout: %1").arg(s));
}


void MainWindow::printConsole(const QString &s) {
    lineNum++;
    ui->consoleOutput->document()->setPlainText(ui->consoleOutput->toPlainText() + tr("Ln[%1]:  %2\n").arg(lineNum).arg(s));
}

void MainWindow::on_consoleInput_returnPressed() {
    u_char input = ui->consoleInput->text().toInt();
    u_char output = '\n';
//    comms.send_and_receive(input, output, 3000);
    printConsole(QString("%1").arg(output));
    ui->consoleInput->setText("");
}

void MainWindow::on_autoStartBtn_clicked() {
    auto result = comms.WriteCoil(commsconfig::AUTO_SEQ, true);
    if(result.error != commsconfig::NO_ERROR) {
        printConsole(QString::fromStdString(
                std::string("ERROR: ") + commsconfig::ErrorCode[result.error]));
    } else {
        if(watch->isRunning()) {
        }
        else {
            watch->start();
        }
    }
}

void MainWindow::on_autoStopBtn_clicked() {
    auto result = comms.WriteCoil(commsconfig::AUTO_SEQ, false);
    if(result.error != commsconfig::NO_ERROR) {
        printConsole(QString::fromStdString(
                std::string("ERROR: ") + commsconfig::ErrorCode[result.error]));
    } else {
        ui->LCDWatch->display(0);
        watch->reset();
    }
}

void MainWindow::on_valveHelium_clicked() {
    if (valveHeliumOpen) {
        auto result = comms.WriteCoil(commsconfig::PRES_SERVO, false);
        if(result.error != commsconfig::NO_ERROR) {
            printConsole(QString::fromStdString(
                    std::string("ERROR: ") + commsconfig::ErrorCode[result.error]));
            return;
        }
        ui->valveHelium->setStyleSheet("border-image: url(:/images/valve_closed_side.png) 0 0 0 0 stretch stretch;\nbackground-color: rgba(255, 255, 255, 0);");
        if (valveFuelVentOpen) {
            ui->lineHelium1->setPixmap(QPixmap(":/images/line_clear.png"));
            ui->lineHelium2->setPixmap(QPixmap(":/images/line_clear_side.png"));
            ui->lineHelium3->setPixmap(QPixmap(":/images/line_clear.png"));
        }
        valveHeliumOpen = false;
    } else {
        auto result = comms.WriteCoil(commsconfig::PRES_SERVO, true);
        if(result.error != commsconfig::NO_ERROR) {
            printConsole(QString::fromStdString(
                    std::string("ERROR: ") + commsconfig::ErrorCode[result.error]));
            return;
        }
        ui->valveHelium->setStyleSheet("border-image: url(:/images/valve_opened_side.png) 0 0 0 0 stretch stretch;\nbackground-color: rgba(255, 255, 255, 0);");
        ui->lineHelium1->setPixmap(QPixmap(":/images/line_helium.png"));
        ui->lineHelium2->setPixmap(QPixmap(":/images/line_helium_side.png"));
        ui->lineHelium3->setPixmap(QPixmap(":/images/line_helium.png"));
        if (valveFuelVentOpen) {
                ui->lineFuelFill->setPixmap(QPixmap(":/images/line_helium_side.png"));
        }
        if (fuelInLines) {
            ui->lineFuel1->setPixmap(QPixmap(":/images/line_water.png"));
            ui->lineFuel2->setPixmap(QPixmap(":/images/line_water_side.png"));
            if (valveFuelMainOpen) {
                ui->lineFuel3->setPixmap(QPixmap(":/images/line_water_side.png"));
            }
        } else {
            ui->lineFuel1->setPixmap(QPixmap(":/images/line_helium.png"));
            ui->lineFuel2->setPixmap(QPixmap(":/images/line_helium_side.png"));
            if (valveFuelMainOpen) {
                ui->lineFuel3->setPixmap(QPixmap(":/images/line_helium_side.png"));
            }
        }
        heliumInLines = true;
        valveHeliumOpen = true;
    }
}

void MainWindow::on_valveFuelVent_clicked() {
    if (valveFuelVentOpen) {
        auto result = comms.WriteCoil(commsconfig::FUEL_FILL_SERVO, false);
        if(result.error != commsconfig::NO_ERROR) {
            printConsole(QString::fromStdString(
                    std::string("ERROR: ") + commsconfig::ErrorCode[result.error]));
            return;
        }
        ui->valveFuelVent->setStyleSheet("border-image: url(:/images/valve_closed_up.png) 0 0 0 0 stretch stretch;\nbackground-color: rgba(255, 255, 255, 0);");
        valveFuelVentOpen = false;
        if (valveHeliumOpen) {
            ui->lineHelium1->setPixmap(QPixmap(":/images/line_helium.png"));
            ui->lineHelium2->setPixmap(QPixmap(":/images/line_helium_side.png"));
            ui->lineHelium3->setPixmap(QPixmap(":/images/line_helium.png"));
        }
        ui->lineFuelFill->setPixmap(QPixmap(":/images/line_clear_side.png"));
    } else {
        auto result = comms.WriteCoil(commsconfig::FUEL_FILL_SERVO, true);
        if(result.error != commsconfig::NO_ERROR) {
            printConsole(QString::fromStdString(
                    std::string("ERROR: ") + commsconfig::ErrorCode[result.error]));
            return;
        }
        ui->valveFuelVent->setStyleSheet("border-image: url(:/images/valve_opened_up.png) 0 0 0 0 stretch stretch;\nbackground-color: rgba(255, 255, 255, 0);");
        if (heliumInLines && !valveHeliumOpen) {
            ui->lineHelium1->setPixmap(QPixmap(":/images/line_clear.png"));
            ui->lineHelium2->setPixmap(QPixmap(":/images/line_clear_side.png"));
            ui->lineHelium3->setPixmap(QPixmap(":/images/line_clear.png"));
            heliumInLines = false;
            if (!fuelInLines) {
                ui->lineFuel1->setPixmap(QPixmap(":/images/line_clear.png"));
                ui->lineFuel2->setPixmap(QPixmap(":/images/line_clear_side.png"));
            }
        }
        valveFuelVentOpen = true;
    }
}

void MainWindow::on_valveFuelMain_clicked() {
    if (valveFuelMainOpen) {
        auto result = comms.WriteCoil(commsconfig::FUEL_MAIN_SERVO, false);
        if(result.error != commsconfig::NO_ERROR) {
            printConsole(QString::fromStdString(
                    std::string("ERROR: ") + commsconfig::ErrorCode[result.error]));
            return;
        }
        ui->valveFuelMain->setStyleSheet("border-image: url(:/images/valve_closed_up.png) 0 0 0 0 stretch stretch;\nbackground-color: rgba(255, 255, 255, 0);");
        ui->lineFuel3->setPixmap(QPixmap(":/images/line_clear_side.png"));
        valveFuelMainOpen = false;
    } else {
        auto result = comms.WriteCoil(commsconfig::FUEL_MAIN_SERVO, true);
        if(result.error != commsconfig::NO_ERROR) {
            printConsole(QString::fromStdString(
                    std::string("ERROR: ") + commsconfig::ErrorCode[result.error]));
            return;
        }
        ui->valveFuelMain->setStyleSheet("border-image: url(:/images/valve_opened_up.png) 0 0 0 0 stretch stretch;\nbackground-color: rgba(255, 255, 255, 0);");
        if (heliumInLines && fuelInLines) {
            ui->lineFuel3->setPixmap(QPixmap(":/images/line_water_side.png"));
        }
        if (heliumInLines && !valveHeliumOpen && valveFuelVentOpen) {
            ui->lineHelium1->setPixmap(QPixmap(":/images/line_clear.png"));
            ui->lineHelium2->setPixmap(QPixmap(":/images/line_clear_side.png"));\
            ui->lineHelium3->setPixmap(QPixmap(":/images/line_clear.png"));
        }
        valveFuelMainOpen = true;
    }
}

void MainWindow::on_fillTankBtn_clicked() {
    if (valveFuelVentOpen) {
        fuelInLines = true;
        ui->lineFuel1->setPixmap(QPixmap(":/images/line_water.png"));
        ui->lineFuel2->setPixmap(QPixmap(":/images/line_water_side.png"));
    }
}

void MainWindow::on_autoStartBtn_2_clicked() {
    auto result = comms.WriteCoil(commsconfig::AUTO_SEQ_DEP, true);
    if(result.error != commsconfig::NO_ERROR) {
        printConsole(QString::fromStdString(
                std::string("ERROR: ") + commsconfig::ErrorCode[result.error]));
        return;
    } else {
        if(watch->isRunning()) {
        }
        else {
            watch->start();
        }
    }
}

void MainWindow::on_unlockBtn_clicked()
{
    ui->consoleInput->setEnabled(true);
}

std::string MainWindow::GenerateTimestampedFilename(const std::string& filename) {
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::ostringstream oss;
    oss << std::put_time(&tm, "%Y%m%d-%H%M%S_") << filename;
    return oss.str();
}
