#ifndef COMMS_H
#define COMMS_H

#include <fstream>
#include <iostream>
#include <sstream>
#include <thread>
#include <memory>
#include <chrono>
#include <string>
#include <utility>
#include <vector>
#include <cerrno>
#include <mutex>
#include <iomanip>
#include <ctime>
#include <future>

#include "serial/serial.h"

#include "CommsConfig.h"

/**
 * Datastructure to represent a coil being communicated over the connection.
 */
struct Coil {
    const commsconfig::CoilConfig coil;
    const commsconfig::ErrorConfig error;
    const bool value;

    Coil(commsconfig::ErrorConfig error, commsconfig::CoilConfig coil, bool value)
        : error(error), coil(coil), value(value) {}
    explicit Coil(commsconfig::ErrorConfig error)
        : Coil(error, commsconfig::END_COILS, false) {}
};

/**
 * Datastructure to represent a holding register being communicated over the connection.
 */
struct HoldingRegister {
    const commsconfig::HoldingRegConfig reg;
    const commsconfig::ErrorConfig error;
    const int value;

    HoldingRegister(commsconfig::ErrorConfig error, commsconfig::HoldingRegConfig reg, int value)
        : error(error), reg(reg), value(value) {}
    explicit HoldingRegister(commsconfig::ErrorConfig error)
        : HoldingRegister(error, commsconfig::END_HOLDING_REGS, false) {}
};

/**
 * Datastructure to represent an input register being communicated over the connection.
 */
struct InputRegister {
    const commsconfig::InputRegConfig reg;
    const commsconfig::ErrorConfig error;
    const int value;

    InputRegister(commsconfig::ErrorConfig error, commsconfig::InputRegConfig reg, int value)
        : error(error), reg(reg), value(value) {}
    explicit InputRegister(commsconfig::ErrorConfig error)
        : InputRegister(error, commsconfig::END_INPUT_REGS, false) {}
};

/**
 * An object to handle all communications with the embedded avionics infrastructure through a
 * serial port. The current infrastructure uses a custom protocol designed with data structures
 * similar to modbus (ie. coils, input registers and holding registers) without the large
 * storage overhead of the modbus library and with extended functionality to allow for streaming
 * data over the channel.
 */
class Comms {
    /**
     * The serial device to communicate through.
     */
     std::unique_ptr<serial::Serial> serial_device_ptr;
    /**
     * Mutex to protect this object and the communications bus from concurrent activity.
     */
    std::mutex comms_mutex;
    /**
     * File to write the log of the communications to.
     */
    std::ofstream log_file_stream;
    /**
     * Mutex to protect access to the log file.
     */
    std::mutex log_mutex;

    /**
     * Helper class to contain the information about the result of a communication.
     */
    struct CommsResult {
        const commsconfig::ErrorConfig error_status;
        const std::vector<std::string> response_arguments;
        explicit CommsResult(commsconfig::ErrorConfig error_status,
                             std::vector<std::string> response_arguments = {})
                :   error_status(error_status),
                    response_arguments(std::move(response_arguments)) {}
    };

public:
    /**
     * Constructs a comms object using a user defined file to log to.
     * @param log_file This is the file to log events to.
     * aborting.
     */
    explicit Comms(const std::string& log_file_location = "./");

    /**
     * Attempts a connection with the given serial port. This also sets the port of the instance
     * to the port given to this function.
     * @return An error code indicating success (NO_ERROR) or a failure with the specific error.
     */
    commsconfig::ErrorConfig Connect(const std::string& new_port);

    /**
     * Disconnects the serial device. This means that no further reads or writes can be performed.
     */
     void Disconnect();

     /**
      * Returns whether we are currently connected to a serial device.
      * @return True is we are connected.
      */
     bool IsConnected() {
         return serial_device_ptr and serial_device_ptr->isOpen();
     }


    /**
     * Returns whether we are currently streaming or not.
     * @return True if we are streaming.
     */
    bool IsBusy() {
        return not std::unique_lock<std::mutex>(comms_mutex, std::try_to_lock_t());
    }

    /**
     * Performs a stress test on the communication channel to determine its capacity and returns
     * this result as a string. The actions performed are also logged to a file.
     * @param test_duration The number of milliseconds to run the test for.
     * @return The result of the stress test as a string.
     */
    std::string TestConnection(int test_duration);

    /**
     * Reads from a coil and returns the coil data. This is a blocking operation.
     * @param coil The coil to read from. Defined in CommsConfig.h.
     * @return A Coil object containing information about the coil and whether it was
     * successfully read.
     */
    Coil ReadCoil(commsconfig::CoilConfig coil);
    /**
     * Writes to a coil and returns the coil data.  This is a blocking operation.
     * @param coil The coil to read from. Defined in CommsConfig.h.
     * @param write_value The value to write to the coil.
     * @return A Coil object containing information about the coil and whether it was
     * successfully written.
     */
    Coil WriteCoil(commsconfig::CoilConfig coil, bool write_value);
    /**
     * Reads from a holding register and returns the holding register data. This is a blocking
     * operation.
     * @param reg The holding register to read from. Defined in CommsConfig.h.
     * @return A HoldingRegister object containing information about the holding register and
     * whether it was successfully read.
     */
    HoldingRegister ReadHoldingRegister(commsconfig::HoldingRegConfig reg);
    /**
     * Writes to a holding register and returns the holding register data.  This is a blocking
     * operation.
     * @param reg The holding register to read from. Defined in CommsConfig.h.
     * @param write_value The value to write to the holding register.
     * @return A HoldingRegister object containing information about the holding register and
     * whether it was successfully written.
     */
    HoldingRegister WriteHoldingRegister(commsconfig::HoldingRegConfig reg, int write_value);
    /**
     * Reads from an input register and returns the holding register data. This is a blocking
     * operation.
     * @param reg The input register to read from. Defined in CommsConfig.h.
     * @return A InputRegister object containing information about the input register and
     * whether it was successfully read.
     */
    InputRegister ReadInputRegister(commsconfig::InputRegConfig reg);
    /**
     * Streams data from the input registers into the output file for a duration with data read
     * at a frequency. This is a non-blocking operation however no other communications can be
     * performed until this function completes.
     *
     * This function will block and should be run concurrently.
     *
     * @param dump_file The file that the stream will be dumped into.
     * @param registers The registers to be streamed.
     * @param stream_duration The duration in milliseconds that the stream will be open for.
     * @param stream_period The period between each data point being streamed in milliseconds
     * (for example an argument of 10 would mean a 10ms period between data points or a data
     * collections rate of 100Hz.
     * @return A comms result that indicates whether an error occurred or that the stream has
     * been started successfully.
     */
    CommsResult StreamInputRegisters(const std::string& dump_file,
                              const std::vector<commsconfig::InputRegConfig>& registers,
                              int stream_duration, int stream_period);

private:

    /**
     * Refreshes the serial device by flushing the buffers and reading anything in the input.
     */
    void RefreshSerialDevice();
    /**
     * Sends a command on the comms line and waits to receive a response. If there are any errors
     * in this process it will detect them and report them in the CommsResult returned.
     *
     * @param command_arguments The arguments of the command to be sent.
     * @param num_response_args The number of arguments that are expected in the response.
     * @param args_to_check The number of arguments to check to verify the message.
     * @return The result of the communication.
     */
    CommsResult SendAndReceive(const std::vector<std::string>& command_arguments,
                               int num_response_args, int args_to_check);
    /**
     * Streams data from the input registers into the output file for a duration with data read
     * at a frequency. This is a non-blocking operation however no other communications can be
     * performed until this function completes.
     *
     * This function will blocks and should be run concurrently.
     *
     * @param dump_file The file that the stream will be dumped into.
     * @param registers The registers to be streamed.
     * @param stream_duration The duration in milliseconds that the stream will be open for.
     * @param stream_period The period between each data point being streamed in milliseconds
     * (for example an argument of 10 would mean a 10ms period between data points or a data
     * collections rate of 100Hz.
     * @param result_promise A promise that will be set to indicate whether the command was
     * successfully sent and the devices are now streaming or not.
     */
    void StreamInputRegistersInThread(const std::string& dump_file,
                                      const std::vector<commsconfig::InputRegConfig>& registers,
                                      std::promise<CommsResult> result_promise,
                                      int stream_duration, int stream_period);
    /**
     * Packs a command with ambles and tries to send it to the serial device.
     * @param command_arguments Arguments of the command to send.
     * @return If the packed message was successfully sent returns true.
     */
    bool PackAndSendCommand(const std::vector<std::string>& command_arguments);
    /**
     * Tries to read a message until the timeout expires. A message will be terminated by a
     * newline character.
     * @param response The string to write the received message to.
     * @return True if the message was successfully received and false otherwise. The only reason
     * this function fails is if the timeout is exceeded.
     */
    bool ReceiveMessage(std::string& response);
    /**
     * Processes a response message that has been read in by ReceiveMessage. This includes checks
     * on the ambles, whether an error was received, whether the correct number of arguments were
     * in the response, and whether the arguments in the response were correct. If there are no
     * errors detected then the arguments are written to a CommsResult object.
     *
     * @param response_message The response packet read by ReceiveMessage.
     * @param command_arguments The arguments that were sent.
     * @param num_response_args The number of arguments to be expected in the response.
     * @param args_to_check The arguments from the sent message that need to be checked in the
     * received message.
     * @return A CommsResult object either indicating an error or, if successful, indicating a no
     * error and the arguments received in the message.
     */
    CommsResult ProcessResponseMessage(std::string& response_message,
                                       const std::vector<std::string>& command_arguments,
                                       int num_response_args, int args_to_check);
    /**
     * Strips a
     * @param message The message to strip.
     * @param command A String to store the command in if it is successfully stripped from the
     * message.
     * @param perform_check Flag to indicate whether to check the ambles.
     * @return True if the message was stripped successfully, false otherwise.
     */
    bool StripMessage(const std::string& message, std::string& command, bool perform_check = true);
    static void ExtractResponseArguments(const std::string& response,
                                  std::vector<std::string>& response_arguments);
    /**
     * Checks the format of a response to make sure that it is as expected.
     * @param arguments
     * @param response_arguments
     * @param args_to_check
     * @return
     */
    static bool CheckResponse(const std::vector<std::string>& arguments,
                       const std::vector<std::string>& response_arguments,
                       int args_to_check);
    /**
     * Gets the number of milliseconds that have elapsed since start.
     * @param start The time to compare to.
     * @return The number of milliseconds as an int that have elapsed.
     */
    static int GetElapsedMilliseconds(std::chrono::steady_clock::time_point start);

    /**
     * Logs an error using the error config code.
     * @param error The error config code to use when logging the error.
     */
    void LogError(commsconfig::ErrorConfig error, const std::string& additional_info = "");
    /**
     * Logs an error using a message describing the error.
     * @param error_message A message describing the error.
     */
    void LogError(const std::string& error_message);
    /**
     * Logs the information about a message that was sent over the comms channel.
     * @param message_sent The message that was sent.
     */
    void LogMessageSend(const std::string& message_sent);
    /**
     * Logs the information about a message that was received from the comms channel.
     * @param message_response The message that was received.
     */
    void LogMessageResponse(const std::string& message_response);
    /**
     * Logs a general message containing some information about the comms.
     * @param message The message to log.
     */
    void LogMessage(const std::string& message);
    /**
     * Writes a string to the log file prepending a timestamp to the message.
     * @param string The string to be written.
     */
    void LogWrite(const std::string& string);
};

#endif // COMMS_H
