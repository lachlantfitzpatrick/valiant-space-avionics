/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QLabel *label_2;
    QPushButton *valveHelium;
    QLabel *label_3;
    QLabel *lineHelium1;
    QLabel *lineHelium2;
    QPushButton *valveFuelVent;
    QLabel *lineFuelFill;
    QLabel *lineHelium3;
    QLabel *label_8;
    QLabel *lineFuel1;
    QLabel *lineFuel2;
    QLabel *lineFuel3;
    QPushButton *valveFuelMain;
    QLabel *label_12;
    QPushButton *autoStartBtn;
    QPushButton *autoStopBtn;
    QFrame *line;
    QComboBox *portSelectBox;
    QPushButton *connectBtn;
    QLineEdit *consoleInput;
    QPushButton *unlockBtn;
    QPushButton *fillTankBtn;
    QPlainTextEdit *consoleOutput;
    QLCDNumber *LCDWatch;
    QLabel *label_4;
    QPushButton *autoStartBtn_2;
    QLabel *label_5;
    QLabel *label_6;
    QLCDNumber *tankPressVal;
    QPushButton *testBtn;
    QPushButton *disconnectBtn;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1056, 746);
        MainWindow->setStyleSheet(QStringLiteral("QMainWindow > QWidget { background-color: rgb(0, 0, 0);} "));
        MainWindow->setAnimated(true);
        MainWindow->setDockOptions(QMainWindow::AllowTabbedDocks|QMainWindow::AnimatedDocks);
        MainWindow->setUnifiedTitleAndToolBarOnMac(false);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(750, 5, 71, 146));
        QFont font;
        font.setFamily(QStringLiteral("Arial"));
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setAutoFillBackground(false);
        label->setStyleSheet(QLatin1String("background-color: rgb(0, 107, 0);\n"
"color: rgb(255, 255, 255);"));
        label->setAlignment(Qt::AlignCenter);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(920, 125, 21, 21));
        label_2->setPixmap(QPixmap(QString::fromUtf8("images/valve_closed.png")));
        label_2->setScaledContents(true);
        valveHelium = new QPushButton(centralWidget);
        valveHelium->setObjectName(QStringLiteral("valveHelium"));
        valveHelium->setGeometry(QRect(747, 200, 66, 66));
        valveHelium->setStyleSheet(QLatin1String("border-image: url(:/images/valve_closed_side.png) 0 0 0 0 stretch stretch;\n"
"background-color: rgba(255, 255, 255, 0);"));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(765, 151, 40, 86));
        label_3->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/images/line_helium.png")));
        label_3->setScaledContents(true);
        lineHelium1 = new QLabel(centralWidget);
        lineHelium1->setObjectName(QStringLiteral("lineHelium1"));
        lineHelium1->setGeometry(QRect(765, 225, 40, 81));
        lineHelium1->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        lineHelium1->setPixmap(QPixmap(QString::fromUtf8(":/images/line_clear.png")));
        lineHelium1->setScaledContents(true);
        lineHelium2 = new QLabel(centralWidget);
        lineHelium2->setObjectName(QStringLiteral("lineHelium2"));
        lineHelium2->setGeometry(QRect(588, 282, 201, 40));
        lineHelium2->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        lineHelium2->setPixmap(QPixmap(QString::fromUtf8(":/images/line_clear_side.png")));
        lineHelium2->setScaledContents(true);
        valveFuelVent = new QPushButton(centralWidget);
        valveFuelVent->setObjectName(QStringLiteral("valveFuelVent"));
        valveFuelVent->setGeometry(QRect(555, 264, 66, 66));
        valveFuelVent->setStyleSheet(QLatin1String("border-image: url(:/images/valve_closed_up.png) 0 0 0 0 stretch stretch;\n"
"background-color: rgba(255, 255, 255, 0);"));
        lineFuelFill = new QLabel(centralWidget);
        lineFuelFill->setObjectName(QStringLiteral("lineFuelFill"));
        lineFuelFill->setGeometry(QRect(490, 282, 101, 40));
        lineFuelFill->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        lineFuelFill->setPixmap(QPixmap(QString::fromUtf8(":/images/line_clear_side.png")));
        lineFuelFill->setScaledContents(true);
        lineHelium3 = new QLabel(centralWidget);
        lineHelium3->setObjectName(QStringLiteral("lineHelium3"));
        lineHelium3->setGeometry(QRect(685, 300, 40, 96));
        lineHelium3->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        lineHelium3->setPixmap(QPixmap(QString::fromUtf8(":/images/line_clear.png")));
        lineHelium3->setScaledContents(true);
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(670, 395, 71, 146));
        label_8->setFont(font);
        label_8->setAutoFillBackground(false);
        label_8->setStyleSheet(QLatin1String("background-color: rgb(0, 166, 158);\n"
"color: rgb(255, 255, 255);\n"
"border-color: rgb(255, 0, 0);"));
        label_8->setAlignment(Qt::AlignCenter);
        lineFuel1 = new QLabel(centralWidget);
        lineFuel1->setObjectName(QStringLiteral("lineFuel1"));
        lineFuel1->setGeometry(QRect(685, 541, 40, 81));
        lineFuel1->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        lineFuel1->setPixmap(QPixmap(QString::fromUtf8(":/images/line_clear.png")));
        lineFuel1->setScaledContents(true);
        lineFuel2 = new QLabel(centralWidget);
        lineFuel2->setObjectName(QStringLiteral("lineFuel2"));
        lineFuel2->setGeometry(QRect(701, 600, 86, 40));
        lineFuel2->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        lineFuel2->setPixmap(QPixmap(QString::fromUtf8(":/images/line_clear_side.png")));
        lineFuel2->setScaledContents(true);
        lineFuel3 = new QLabel(centralWidget);
        lineFuel3->setObjectName(QStringLiteral("lineFuel3"));
        lineFuel3->setGeometry(QRect(770, 600, 126, 40));
        lineFuel3->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        lineFuel3->setPixmap(QPixmap(QString::fromUtf8(":/images/line_clear_side.png")));
        lineFuel3->setScaledContents(true);
        valveFuelMain = new QPushButton(centralWidget);
        valveFuelMain->setObjectName(QStringLiteral("valveFuelMain"));
        valveFuelMain->setGeometry(QRect(745, 582, 66, 66));
        valveFuelMain->setStyleSheet(QLatin1String("border-image: url(:/images/valve_closed_up.png) 0 0 0 0 stretch stretch;\n"
"background-color: rgba(255, 255, 255, 0);"));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(895, 595, 125, 50));
        label_12->setStyleSheet(QStringLiteral("background-color: rgba(255, 255, 255, 0);"));
        label_12->setPixmap(QPixmap(QString::fromUtf8(":/images/rocket.png")));
        label_12->setScaledContents(true);
        autoStartBtn = new QPushButton(centralWidget);
        autoStartBtn->setObjectName(QStringLiteral("autoStartBtn"));
        autoStartBtn->setGeometry(QRect(15, 370, 231, 61));
        QFont font1;
        font1.setFamily(QStringLiteral("Arial"));
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        autoStartBtn->setFont(font1);
        autoStartBtn->setStyleSheet(QLatin1String("background-color: rgb(255, 244, 0);\n"
"color: rgb(0, 0, 0);"));
        autoStartBtn->setCheckable(false);
        autoStopBtn = new QPushButton(centralWidget);
        autoStopBtn->setObjectName(QStringLiteral("autoStopBtn"));
        autoStopBtn->setGeometry(QRect(245, 370, 176, 121));
        autoStopBtn->setFont(font1);
        autoStopBtn->setAutoFillBackground(false);
        autoStopBtn->setStyleSheet(QLatin1String("background: red;\n"
"color: rgb(0, 0, 0);"));
        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(415, 5, 31, 696));
        line->setAutoFillBackground(false);
        line->setStyleSheet(QStringLiteral("color: white;"));
        line->setFrameShadow(QFrame::Plain);
        line->setLineWidth(2);
        line->setMidLineWidth(0);
        line->setFrameShape(QFrame::VLine);
        portSelectBox = new QComboBox(centralWidget);
        portSelectBox->setObjectName(QStringLiteral("portSelectBox"));
        portSelectBox->setGeometry(QRect(25, 10, 301, 32));
        portSelectBox->setStyleSheet(QStringLiteral(""));
        connectBtn = new QPushButton(centralWidget);
        connectBtn->setObjectName(QStringLiteral("connectBtn"));
        connectBtn->setGeometry(QRect(330, 10, 81, 31));
        QFont font2;
        font2.setFamily(QStringLiteral("Arial"));
        font2.setPointSize(11);
        font2.setBold(true);
        font2.setWeight(75);
        connectBtn->setFont(font2);
        connectBtn->setStyleSheet(QStringLiteral(""));
        connectBtn->setCheckable(false);
        consoleInput = new QLineEdit(centralWidget);
        consoleInput->setObjectName(QStringLiteral("consoleInput"));
        consoleInput->setEnabled(false);
        consoleInput->setGeometry(QRect(30, 290, 286, 21));
        consoleInput->setStyleSheet(QStringLiteral(""));
        unlockBtn = new QPushButton(centralWidget);
        unlockBtn->setObjectName(QStringLiteral("unlockBtn"));
        unlockBtn->setGeometry(QRect(325, 286, 86, 31));
        unlockBtn->setFont(font2);
        unlockBtn->setStyleSheet(QStringLiteral(""));
        unlockBtn->setCheckable(false);
        fillTankBtn = new QPushButton(centralWidget);
        fillTankBtn->setObjectName(QStringLiteral("fillTankBtn"));
        fillTankBtn->setEnabled(true);
        fillTankBtn->setGeometry(QRect(450, 261, 86, 31));
        fillTankBtn->setStyleSheet(QStringLiteral(""));
        consoleOutput = new QPlainTextEdit(centralWidget);
        consoleOutput->setObjectName(QStringLiteral("consoleOutput"));
        consoleOutput->setGeometry(QRect(30, 45, 366, 236));
        LCDWatch = new QLCDNumber(centralWidget);
        LCDWatch->setObjectName(QStringLiteral("LCDWatch"));
        LCDWatch->setGeometry(QRect(95, 510, 236, 91));
        LCDWatch->setStyleSheet(QStringLiteral("color: rgb(255, 255, 255);"));
        LCDWatch->setSmallDecimalPoint(false);
        LCDWatch->setDigitCount(4);
        LCDWatch->setMode(QLCDNumber::Dec);
        LCDWatch->setSegmentStyle(QLCDNumber::Flat);
        LCDWatch->setProperty("value", QVariant(0));
        LCDWatch->setProperty("intValue", QVariant(0));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(80, 500, 256, 111));
        QFont font3;
        font3.setFamily(QStringLiteral("Arial"));
        font3.setPointSize(100);
        font3.setBold(true);
        font3.setWeight(75);
        label_4->setFont(font3);
        label_4->setAutoFillBackground(false);
        label_4->setStyleSheet(QLatin1String("background-color: rgb(61, 62, 62);\n"
"color: rgb(255, 255, 255);"));
        label_4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        autoStartBtn_2 = new QPushButton(centralWidget);
        autoStartBtn_2->setObjectName(QStringLiteral("autoStartBtn_2"));
        autoStartBtn_2->setGeometry(QRect(15, 430, 231, 61));
        autoStartBtn_2->setFont(font1);
        autoStartBtn_2->setStyleSheet(QLatin1String("background-color: rgb(255, 244, 0);\n"
"color: rgb(0, 0, 0);"));
        autoStartBtn_2->setCheckable(false);
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(460, 170, 276, 41));
        label_5->setFont(font);
        label_5->setAutoFillBackground(false);
        label_5->setStyleSheet(QLatin1String("background-color: rgb(200, 0, 0);\n"
"color: rgb(255, 255, 255);"));
        label_5->setAlignment(Qt::AlignCenter);
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(775, 530, 276, 41));
        label_6->setFont(font);
        label_6->setAutoFillBackground(false);
        label_6->setStyleSheet(QLatin1String("background-color: rgb(200, 0, 0);\n"
"color: rgb(255, 255, 255);"));
        label_6->setAlignment(Qt::AlignCenter);
        tankPressVal = new QLCDNumber(centralWidget);
        tankPressVal->setObjectName(QStringLiteral("tankPressVal"));
        tankPressVal->setGeometry(QRect(790, 320, 236, 91));
        tankPressVal->setStyleSheet(QStringLiteral("color: rgb(255, 255, 255);"));
        tankPressVal->setSmallDecimalPoint(false);
        tankPressVal->setDigitCount(10);
        tankPressVal->setMode(QLCDNumber::Dec);
        tankPressVal->setSegmentStyle(QLCDNumber::Flat);
        tankPressVal->setProperty("value", QVariant(600));
        tankPressVal->setProperty("intValue", QVariant(600));
        testBtn = new QPushButton(centralWidget);
        testBtn->setObjectName(QStringLiteral("testBtn"));
        testBtn->setGeometry(QRect(245, 325, 111, 31));
        testBtn->setFont(font2);
        testBtn->setStyleSheet(QStringLiteral(""));
        testBtn->setCheckable(false);
        disconnectBtn = new QPushButton(centralWidget);
        disconnectBtn->setObjectName(QStringLiteral("disconnectBtn"));
        disconnectBtn->setGeometry(QRect(110, 325, 111, 31));
        disconnectBtn->setFont(font2);
        disconnectBtn->setStyleSheet(QStringLiteral(""));
        disconnectBtn->setCheckable(false);
        MainWindow->setCentralWidget(centralWidget);
        label_4->raise();
        lineFuelFill->raise();
        lineHelium1->raise();
        label->raise();
        label_2->raise();
        label_3->raise();
        lineHelium2->raise();
        valveFuelVent->raise();
        lineHelium3->raise();
        label_8->raise();
        lineFuel1->raise();
        lineFuel2->raise();
        lineFuel3->raise();
        valveFuelMain->raise();
        label_12->raise();
        autoStartBtn->raise();
        autoStopBtn->raise();
        valveHelium->raise();
        line->raise();
        portSelectBox->raise();
        connectBtn->raise();
        consoleInput->raise();
        unlockBtn->raise();
        fillTankBtn->raise();
        consoleOutput->raise();
        LCDWatch->raise();
        autoStartBtn_2->raise();
        label_5->raise();
        label_6->raise();
        tankPressVal->raise();
        testBtn->raise();
        disconnectBtn->raise();
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1056, 20));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Rocket Control Panel", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "HELIUM", Q_NULLPTR));
        label_2->setText(QString());
        valveHelium->setText(QString());
        label_3->setText(QString());
        lineHelium1->setText(QString());
        lineHelium2->setText(QString());
        valveFuelVent->setText(QString());
        lineFuelFill->setText(QString());
        lineHelium3->setText(QString());
        label_8->setText(QApplication::translate("MainWindow", "FUEL", Q_NULLPTR));
        lineFuel1->setText(QString());
        lineFuel2->setText(QString());
        lineFuel3->setText(QString());
        valveFuelMain->setText(QString());
        label_12->setText(QString());
        autoStartBtn->setText(QApplication::translate("MainWindow", "AUTO START DEP", Q_NULLPTR));
        autoStopBtn->setText(QApplication::translate("MainWindow", "AUTO STOP", Q_NULLPTR));
        connectBtn->setText(QApplication::translate("MainWindow", "CONNECT", Q_NULLPTR));
        unlockBtn->setText(QApplication::translate("MainWindow", "UNLOCK", Q_NULLPTR));
        fillTankBtn->setText(QApplication::translate("MainWindow", "FILL TANK", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", " T", Q_NULLPTR));
        autoStartBtn_2->setText(QApplication::translate("MainWindow", "AUTO START NON-DEP", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "CLOSE HELIUM FOR DEPLETION TESTS", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "CLOSE HELIUM FOR DEPLETION TESTS", Q_NULLPTR));
        testBtn->setText(QApplication::translate("MainWindow", "TEST", Q_NULLPTR));
        disconnectBtn->setText(QApplication::translate("MainWindow", "DISCONNECT", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
