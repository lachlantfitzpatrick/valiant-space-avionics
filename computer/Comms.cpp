#include "Comms.h"

Comms::Comms(const std::string& log_file_location) {
    // Open the log file.
    // Get the time.
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::ostringstream oss;
    oss << log_file_location << std::put_time(&tm, "%Y%m%d-%H%M%S") << "_comms.log";
    log_file_stream.open(oss.str(), std::ofstream::out | std::ofstream::app);
}

commsconfig::ErrorConfig Comms::Connect(const std::string& port) {
    // Log a message.
    LogMessage("Connecting to port: " + port);
    // Check whether the serial device is already connected.
    if (serial_device_ptr and serial_device_ptr->isOpen()) {
        // Log a failed attempt to open.
        LogError(commsconfig::FAILED_TO_OPEN,"The Comms object already has an open serial port.");
        // Return that we failed to open.
        return commsconfig::FAILED_TO_OPEN;
    }
    // Try to open the serial port.
    try {
        serial_device_ptr = std::make_unique<serial::Serial>(
                port, commsconfig::kBaud, serial::Timeout::simpleTimeout(commsconfig::kTimeout));
    } catch (serial::IOException& io_exception) {
        // Log a failed attempt to open.
        LogError(commsconfig::FAILED_TO_OPEN, io_exception.what());
        // Return that we failed to open.
        return commsconfig::FAILED_TO_OPEN;
    }
    // Check that the serial port is open.
    if (not serial_device_ptr->isOpen()) {
        // Log a fail to open.
        LogError(commsconfig::FAILED_TO_OPEN);
        // We failed to open the device.
        return commsconfig::FAILED_TO_OPEN;
    }
    // Try to send and receive a message to read a coil (could be any message).
    // Note, '4' is the number of attempts.
    for (int i = 0; i < 4; i++) {
        Coil test_coil = ReadCoil(commsconfig::TEST_COIL);
        // If there is no error then it was successfully read.
        if (test_coil.error == commsconfig::NO_ERROR) {
            // Log that we've successfully connected.
            LogMessage("Connected");
            // Return no error.
            return commsconfig::NO_ERROR;
        }
    }
    // Try one last time to read the coil.
    Coil test_coil = ReadCoil(commsconfig::TEST_COIL);
    // If there is no error then it was successfully read.
    if (test_coil.error == commsconfig::NO_ERROR) {
        // Log that we've successfully connected.
        LogMessage("Connected");
        // Return no error.
        return commsconfig::NO_ERROR;
    }
    else {
        // Set the serial device pointer to be empty.
        serial_device_ptr.reset(nullptr);
        // Log that we failed to connect.
        LogError(commsconfig::FAILED_TO_CONNECT, "Connection unsuccessful.");
        // Return the error of the attempted read.
        return test_coil.error;
    }
}

void Comms::Disconnect() {
    // Check if the serial device is already managing an object.
    if (serial_device_ptr) {
        // Close the serial device.
        serial_device_ptr->close();
        // Log that it was closed.
        LogMessage("Disconnected.");
    }
}

std::string Comms::TestConnection(int test_duration) {
    // Check whether we have a connection.
    if (not IsConnected()) {
        // Log the error.
        LogError(commsconfig::NO_CONNECTION, "Can't test connection. Not connected.");
        // Return the message.
        return "Can't test connection. Not connected.";
    }
    // Log that we are performing a test.
    LogMessage("Commencing stress test.");
    // Get the start time.
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    // Keep sending messages until the duration is complete. Record the statistics over this time.
    int messages_sent = 0;
    int messages_received = 0;
    while (GetElapsedMilliseconds(start) <= test_duration) {
        // Try and read from the test coil.
        Coil test_coil = ReadCoil(commsconfig::TEST_COIL);
        // Record an attempt.
        messages_sent++;
        // Check if the attempt was successful.
        if (test_coil.error == commsconfig::NO_ERROR) messages_received++;
    }
    // Build the result string.
    std::string results(std::to_string(messages_sent) + " messages sent. "
    + std::to_string(messages_received) + " messages received. "
    + std::to_string((messages_received * 100)/(double)messages_sent) + "% message success. "
    + std::to_string((messages_received * 1000) / (double)test_duration)
    + "Hz successful messaging frequency.");
    // Log the results.
    LogMessage("Stress test complete. " + results);
    // Return the results.
    return results;
}

Coil Comms::ReadCoil(commsconfig::CoilConfig coil) {
    // Check whether we have a connection.
    if (not IsConnected()) {
        // Log the error.
        LogError(commsconfig::NO_CONNECTION, "Can't read coil. Not connected.");
        // Return the message.
        return Coil(commsconfig::NO_CONNECTION);
    }
    // Construct the message by building the arguments.
    std::vector<std::string> message_arguments;
    message_arguments.emplace_back(commsconfig::kRead);
    message_arguments.emplace_back(commsconfig::kCoil);
    message_arguments.emplace_back(std::to_string(coil));
    // Send the message.
    auto comms_result = SendAndReceive(
            message_arguments, commsconfig::kReadResponseArgs, commsconfig::kReadArgs);
    // Handle an error in the communication.
    if (comms_result.error_status != commsconfig::NO_ERROR) {
        // Return the error status.
        return Coil(comms_result.error_status);
    }
    else {
        // Try to extract the response value.
        bool response_value;
        std::istringstream response_stream(
                comms_result.response_arguments[commsconfig::kReadDataValueIndex]);
        response_stream >> response_value;
        // Check whether we successfully extracted the value.
        if (response_stream.eof()) {
            // Successfully read the value, so return a coil with it.
            return Coil(comms_result.error_status, coil, response_value);
        }
        else {
            // The data type response was incorrect.
            // Log an error.
            LogError(commsconfig::INCORRECT_DATA_TYPE_RESPONSE);
            // Return an error status.
            return Coil(commsconfig::INCORRECT_DATA_TYPE_RESPONSE);
        }
    }
}

Coil Comms::WriteCoil(commsconfig::CoilConfig coil, bool write_value) {
    // Check whether we have a connection.
    if (not IsConnected()) {
        // Log the error.
        LogError(commsconfig::NO_CONNECTION, "Can't write coil. Not connected.");
        // Return the message.
        return Coil(commsconfig::NO_CONNECTION);
    }
    // Construct the message by building the arguments.
    std::vector<std::string> message_arguments;
    message_arguments.emplace_back(commsconfig::kWrite);
    message_arguments.emplace_back(commsconfig::kCoil);
    message_arguments.emplace_back(std::to_string(coil));
    message_arguments.emplace_back(std::to_string(write_value));
    // Send the message.
    auto comms_result = SendAndReceive(
            message_arguments, commsconfig::kWriteResponseArgs, commsconfig::kWriteArgs);
    // Handle an error in the communication.
    if (comms_result.error_status != commsconfig::NO_ERROR) {
        // Return the error status.
        return Coil(comms_result.error_status);
    }
    else {
        // Try to extract the response value.
        bool response_value;
        std::istringstream response_stream(
                comms_result.response_arguments[commsconfig::kWriteDataValueIndex]);
        response_stream >> response_value;
        // Check whether we successfully extracted the value.
        if (response_stream.eof()) {
            // Check whether the value is correct.
            if (response_value != write_value) {
                // The response value did not match the written value.
                // Log an error.
                LogError(commsconfig::INCORRECT_DATA_VALUE_RESPONSE);
                // Return an error status.
                return Coil(commsconfig::INCORRECT_DATA_VALUE_RESPONSE);
            }
            // Successfully returned the correct value, so return a coil with this value.
            return Coil(comms_result.error_status, coil, response_value);
        }
        else {
            // The data type response was incorrect.
            // Log an error.
            LogError(commsconfig::INCORRECT_DATA_TYPE_RESPONSE);
            // Return an error status.
            return Coil(commsconfig::INCORRECT_DATA_TYPE_RESPONSE);
        }
    }
}

HoldingRegister Comms::ReadHoldingRegister(commsconfig::HoldingRegConfig  reg) {
    // Check whether we have a connection.
    if (not IsConnected()) {
        // Log the error.
        LogError(commsconfig::NO_CONNECTION, "Can't read holding register. Not connected.");
        // Return the message.
        return HoldingRegister(commsconfig::NO_CONNECTION);
    }
    // Construct the message by building the arguments.
    std::vector<std::string> message_arguments;
    message_arguments.emplace_back(commsconfig::kRead);
    message_arguments.emplace_back(commsconfig::kHoldingReg);
    message_arguments.emplace_back(std::to_string(reg));
    // Send the message.
    auto comms_result = SendAndReceive(
            message_arguments, commsconfig::kReadResponseArgs, commsconfig::kReadArgs);
    // Handle an error in the communication.
    if (comms_result.error_status != commsconfig::NO_ERROR) {
        // Return the error status.
        return HoldingRegister(comms_result.error_status);
    }
    else {
        // Try to extract the response value.
        int response_value;
        std::istringstream response_stream(
                comms_result.response_arguments[commsconfig::kReadDataValueIndex]);
        response_stream >> response_value;
        // Check whether we successfully extracted the value.
        if (response_stream.eof()) {
            // Successfully read the value, so return a coil with it.
            return HoldingRegister(comms_result.error_status, reg, response_value);
        }
        else {
            // The data type response was incorrect.
            // Log an error.
            LogError(commsconfig::INCORRECT_DATA_TYPE_RESPONSE);
            // Return an error status.
            return HoldingRegister(commsconfig::INCORRECT_DATA_TYPE_RESPONSE);
        }
    }
}

HoldingRegister Comms::WriteHoldingRegister(commsconfig::HoldingRegConfig reg, int write_value) {
    // Check whether we have a connection.
    if (not IsConnected()) {
        // Log the error.
        LogError(commsconfig::NO_CONNECTION, "Can't write holding register. Not connected.");
        // Return the message.
        return HoldingRegister(commsconfig::NO_CONNECTION);
    }
    // Construct the message by building the arguments.
    std::vector<std::string> message_arguments;
    message_arguments.emplace_back(commsconfig::kWrite);
    message_arguments.emplace_back(commsconfig::kHoldingReg);
    message_arguments.emplace_back(std::to_string(reg));
    message_arguments.emplace_back(std::to_string(write_value));
    // Send the message.
    auto comms_result = SendAndReceive(
            message_arguments, commsconfig::kWriteResponseArgs, commsconfig::kWriteArgs);
    // Handle an error in the communication.
    if (comms_result.error_status != commsconfig::NO_ERROR) {
        // Return the error status.
        return HoldingRegister(comms_result.error_status);
    }
    else {
        // Try to extract the response write_value.
        int response_value;
        std::istringstream response_stream(
                comms_result.response_arguments[commsconfig::kWriteDataValueIndex]);
        response_stream >> response_value;
        // Check whether we successfully extracted the write_value.
        if (response_stream.eof()) {
            // Check whether the write_value is correct.
            if (response_value != write_value) {
                // The response write_value did not match the written write_value.
                // Log an error.
                LogError(commsconfig::INCORRECT_DATA_VALUE_RESPONSE);
                // Return an error status.
                return HoldingRegister(commsconfig::INCORRECT_DATA_VALUE_RESPONSE);
            }
            // Successfully returned a correct value, so return a coil with this value.
            return HoldingRegister(comms_result.error_status, reg, response_value);
        }
        else {
            // The data type response was incorrect.
            // Log an error.
            LogError(commsconfig::INCORRECT_DATA_TYPE_RESPONSE);
            // Return an error status.
            return HoldingRegister(commsconfig::INCORRECT_DATA_TYPE_RESPONSE);
        }
    }
}

InputRegister Comms::ReadInputRegister(commsconfig::InputRegConfig reg) {
    // Check whether we have a connection.
    if (not IsConnected()) {
        // Log the error.
        LogError(commsconfig::NO_CONNECTION, "Can't read input register. Not connected.");
        // Return the message.
        return InputRegister(commsconfig::NO_CONNECTION);
    }
    // Construct the message by building the arguments.
    std::vector<std::string> message_arguments;
    message_arguments.emplace_back(commsconfig::kRead);
    message_arguments.emplace_back(commsconfig::kInputReg);
    message_arguments.emplace_back(std::to_string(reg));
    // Send the message.
    auto comms_result = SendAndReceive(
            message_arguments, commsconfig::kReadResponseArgs, commsconfig::kReadArgs);
    // Handle an error in the communication.
    if (comms_result.error_status != commsconfig::NO_ERROR) {
        // Return the error status.
        return InputRegister(comms_result.error_status);
    }
    else {
        // Try to extract the response value.
        int response_value;
        std::istringstream response_stream(
                comms_result.response_arguments[commsconfig::kReadDataValueIndex]);
        response_stream >> response_value;
        // Check whether we successfully extracted the value.
        if (response_stream.eof()) {
            // Successfully read the value, so return a coil with it.
            return InputRegister(comms_result.error_status, reg, response_value);
        }
        else {
            // The data type response was incorrect.
            // Log an error.
            LogError(commsconfig::INCORRECT_DATA_TYPE_RESPONSE);
            // Return an error status.
            return InputRegister(commsconfig::INCORRECT_DATA_TYPE_RESPONSE);
        }
    }
}

Comms::CommsResult Comms::StreamInputRegisters(const std::string& dump_file,
        const std::vector<commsconfig::InputRegConfig>& registers,
        int stream_duration, int stream_period) {
    // Check whether we have a connection.
    if (not IsConnected()) {
        // Log the error.
        LogError(commsconfig::NO_CONNECTION, "Can't stream registers. Not connected.");
        // Return the message.
        return CommsResult(commsconfig::NO_CONNECTION);
    }
    // Create a promise to return the result of the comms through.
    std::promise<CommsResult> result_promise;
    // Get the future from the promise.
    std::future<CommsResult> result_future = result_promise.get_future();
    // Try to start the stream.
    std::thread thread(&Comms::StreamInputRegistersInThread, this,
                       dump_file, registers, std::move(result_promise), stream_duration,
                       stream_period);
    // Detach the thread as we aren't waiting on the thread to complete.
    thread.detach();
    // Wait for the future value to be ready.
    result_future.wait();
    // Return the result. Note that the stream is probably still running.
    return result_future.get();
}

void Comms::RefreshSerialDevice() {
    // Flush the buffers.
    serial_device_ptr->flush();
    // Read all the input in.
    while (serial_device_ptr->available()) serial_device_ptr->read();
}

Comms::CommsResult Comms::SendAndReceive(const std::vector<std::string>& command_arguments,
                                         int num_response_args, int args_to_check) {
    // Check whether we are currently communicating.
    if (IsBusy()) {
        // We are not allowed to perform any other communications while streaming.
        // Log this as an error.
        LogError(commsconfig::COMMS_BUSY);
        // Return an error.
        return CommsResult(commsconfig::COMMS_BUSY);
    }
    // Take the mutex until this function finishes.
    std::unique_lock<std::mutex> lock(comms_mutex);
    // Refresh the serial device.
    RefreshSerialDevice();
    // Pack and send the message.
    if (!PackAndSendCommand(command_arguments)) {
        // Failed to send the message over serial.
        return CommsResult(commsconfig::FAILED_TO_SEND);
    }
    // Get the response_message.
    std::string response_message;
    if (not ReceiveMessage(response_message)) {
        // We timed out before we read a newline terminated response_message.
        return CommsResult(commsconfig::TIMEOUT);
    }
    // Process the response and return the result.
    return ProcessResponseMessage(
            response_message, command_arguments, num_response_args, args_to_check);
}

void Comms::StreamInputRegistersInThread(const std::string& dump_file,
                                         const std::vector<commsconfig::InputRegConfig>& registers,
                                         std::promise<CommsResult> result_promise,
                                         int stream_duration, int stream_period) {
    // Prepare to lock the mutex.
    std::unique_lock<std::mutex> lock(comms_mutex);
    // Try to lock the mutex and check if we were successful.
    if (not lock) {
        // We are not allowed to perform any other communications while streaming.
        // Log this as an error.
        LogError(commsconfig::COMMS_BUSY);
        // Set the error.
        result_promise.set_value(CommsResult(commsconfig::COMMS_BUSY));
        // End the attempt to stream.
        return;
    }
    // Configure the command to initiate the stream.
    // Add the stream command.
    std::vector<std::string> initiate_stream_command = {commsconfig::kStream};
    // Add the stream duration.
    initiate_stream_command.emplace_back(std::to_string(stream_duration));
    // Add the stream period.
    initiate_stream_command.emplace_back(std::to_string(stream_period));
    // Add all the registers.
    for (auto reg : registers) initiate_stream_command.emplace_back(std::to_string(reg));
    // Refresh the serial device.
    RefreshSerialDevice();
    // Pack and send the response.
    if (!PackAndSendCommand(initiate_stream_command)) {
        // Failed to send the response over serial.
        result_promise.set_value(CommsResult(commsconfig::FAILED_TO_SEND));
        // End the attempt to stream.
        return;
    }
    // Get the response_message.
    std::string response_message;
    if (!ReceiveMessage(response_message)) {
        // We timed out before we read a newline terminated response_message.
        result_promise.set_value(CommsResult(commsconfig::TIMEOUT));
        // End the attempt to stream.
        return;
    }
    // Process the response and get the result.
    auto comms_result = ProcessResponseMessage(response_message, initiate_stream_command,
                                               commsconfig::kStreamArgs + registers.size() - 1,
                                               commsconfig::kStreamResponseArgs);
    // Check whether we got an error.
    bool first_line_read = false;
    if (comms_result.error_status != commsconfig::NO_ERROR) {
        // It is possible that an error has arisen but the stream was successfully opened.
        // Try to read another line as the stream should immediately send the first line of data.
        if (!ReceiveMessage(response_message)) {
            // We timed out before we read a newline terminated response_message so return the
            // original error as we will assume this was the error.
            result_promise.set_value(CommsResult(comms_result.error_status));
            // Log this as an error.
            LogError(comms_result.error_status);
            // End the attempt to stream.
            return;
        }
        // Indicate that we did read the first line.
        first_line_read = true;
    }
    // The stream is open. Set the promise to indicate this.
    result_promise.set_value(CommsResult(commsconfig::NO_ERROR, comms_result.response_arguments));
    // Log that the stream is open.
    LogMessage("Stream started...");
    // Try open the file to push the stream to.
    LogMessage("Opening dump file.");
    std::ofstream stream_file;
    try {
        stream_file.open(dump_file, std::ofstream::out | std::ofstream::app);
        // Some errors such as permission denied errors are not caught. So double check that the
        // file is open.
        if (not stream_file.is_open()) {
            // Throw the ios failure explicitly.
            throw std::ios::failure("File not opened successfully.");
        }
        // Log that we successfully opened the file.
        LogMessage("Opened dump file.");
    } catch (std::ios::failure& ios_failure_1) {
        // Log that we failed to open the file.
        LogError(ios_failure_1.what());
        // Try and open a default dump file.
        LogMessage("Trying to open default dump file.");
        try {
            auto t = std::time(nullptr);
            auto tm = *std::localtime(&t);
            std::ostringstream oss;
            oss << "./" << std::put_time(&tm, "%Y%m%d-%H%M%S") << "_stream.data";
            stream_file.open(oss.str(), std::ofstream::out | std::ofstream::app);
            // Some errors such as permission denied errors are not caught. So double check that the
            // file is open.
            if (not stream_file.is_open()) {
                // Throw the ios failure explicitly.
                throw std::ios::failure("File not opened successfully.");
            }
            // Log success.
            LogMessage("Opened default dump file.");
        } catch(std::ios::failure& ios_failure_2) {
            // Log that we failed to open the file again.
            LogError(ios_failure_2.what());
        }
    }
    // Setup the file if it is open.
    std::string response;
    if (stream_file.is_open()) {
        // Add headers.
        stream_file << "time ";
        for (auto reg : registers) stream_file << std::to_string(reg) << " ";
        stream_file << std::endl;
        // Check whether we've received the first line of the stream.
        if (first_line_read) {
            // Strip the response of the ambles.
            StripMessage(response_message, response);
            // Stream response directly into the file.
            stream_file << response << std::endl;
        }
    }
    // Process the stream until we receive the finish response or we have exceeded the expected time.
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    std::vector<std::string> response_arguments;
    while (GetElapsedMilliseconds(start) < stream_duration * 1.1) {
        // Try to receive a response.
        if (ReceiveMessage(response_message)) {
            // Strip the response of the ambles and don't check for errors.
            StripMessage(response_message, response, false);
            // Extract the response arguments.
            ExtractResponseArguments(response, response_arguments);
            // Check whether the response is the termination response.
            if (response_arguments[0] == std::string(commsconfig::kStream)) {
                // Log that the stream is finished.
                LogMessage("Stream finished successfully by termination command.");
                // Add an endline to the stream file.
                if (stream_file.is_open()) stream_file << std::endl;
                // Return which will automatically unlock the mutex and close the file.
                return;
            }
            // If the dump file is open stream the response into it.
            if (stream_file.is_open()) {
                // Stream response directly into the file.
                stream_file << response << std::endl;
            }
        }
    }
    // Add an endline to the stream file.
    if (stream_file.is_open()) stream_file << std::endl;
    // Log that the stream finished due to a timeout on the stream duration.
    LogMessage("Stream finished by timeout on the stream duration.");
}

bool Comms::PackAndSendCommand(const std::vector<std::string>& command_arguments) {
    // Create the command to send.
    std::string command = command_arguments[0];
    for (unsigned long i = 1; i < command_arguments.size(); i++) {
        command += commsconfig::kDelim + command_arguments[i];
    }
    // Add the preamble and postamble and send the command.
    std::string message =
            commsconfig::kPreambleRem + command + commsconfig::kPostambleRem + "\n";
    // Send the command.
    auto bytes_written = serial_device_ptr->write(message);
    // Check whether the send failed.
    // todo determine the correct size to make this work.
//    if (bytes_written != command.size()) {
//        // The whole command wasn't sent.
//        // Log this as an error.
//        LogError(commsconfig::FAILED_TO_SEND);
//        // Return false signifying a fail.
//        return false;
//    }
//    else {
    // Log a command being sent.
    LogMessageSend(command);
    // The whole command was sent so return true indicating success.
    return true;
//    }
}

bool Comms::ReceiveMessage(std::string& response) {
    // Read the next line of the serial in.
    response = serial_device_ptr->readline();
    // Check whether the timeout was exceeded.
    if (response.back() != '\n') {
        // A full line wasn't read therefore the timeout was exceeded while reading.
        // Log this as an error.
        LogError(commsconfig::TIMEOUT);
        // The timeout was exceeded.
        return false;
    }
    else {
        // A full line was successfully read.
        return true;
    }
}

Comms::CommsResult Comms::ProcessResponseMessage(std::string& response_message,
                                                 const std::vector<std::string>& command_arguments,
                                                 int num_response_args, int args_to_check) {
    // Strip the response from response_message.
    std::string response;
    if (!StripMessage(response_message, response)) {
        // We didn't have the correct ambles.
        return CommsResult(commsconfig::INCORRECT_ADDRESS_FORMAT);
    }
    // Extract the response_message arguments.
    std::vector<std::string> response_arguments;
    ExtractResponseArguments(response, response_arguments);
    // Check the response_message for the error response.
    if (response_arguments[0] == std::string(commsconfig::kError)) {
        // An error was generated during the communication.
        // Log this as an error.
        LogError(commsconfig::ErrorConfig(std::stoi(response_arguments[1])));
        // Return an error.
        return CommsResult(commsconfig::ErrorConfig(std::stoi(response_arguments[1])));
    }
    // Check we got the expected number of response arguments back.
    if (response_arguments.size() != num_response_args) {
        // We didn't have the right number of arguments.
        // Log this as an error.
        LogError(commsconfig::INCORRECT_NUMBER_ARGS);
        // Return an error.
        return CommsResult(commsconfig::INCORRECT_NUMBER_ARGS);
    }
    // Check the response message arguments matched the sent message arguments.
    if (!CheckResponse(command_arguments, response_arguments, args_to_check)) {
        // The response arguments didn't match.
        // Log this as an error.
        LogError(commsconfig::INCORRECT_RESPONSE_FORMAT);
        // Return an error.
        return CommsResult(commsconfig::INCORRECT_RESPONSE_FORMAT);
    }
    // Success.
    return CommsResult(commsconfig::NO_ERROR, response_arguments);
}

bool Comms::StripMessage(const std::string& message, std::string& command, bool perform_check) {
    // Don't worry about checking the ambles.
    if (not perform_check) {/*Skip the else if.*/}
        // Check whether the message has the correct ambles.
    else if (message.substr(0, commsconfig::kAmbleLength) != commsconfig::kPreambleComp or
             message.substr(message.length() - 1 - commsconfig::kAmbleLength, commsconfig::kAmbleLength) != commsconfig::kPostambleComp) {
        // Log this as an error.
        LogError(commsconfig::INCORRECT_ADDRESS_FORMAT);
        // Indicate that this method failed.
        return false;
    }
    // Strip the message.
    command = message.substr(commsconfig::kAmbleLength,
                             message.length() - 1 - 2 * commsconfig::kAmbleLength);
    // Log a command being received.
    LogMessageResponse(command);
    // Return success.
    return true;
}

void Comms::ExtractResponseArguments(const std::string& response,
                                     std::vector<std::string>& response_arguments) {
    // Setup the string stream to process from.
    std::istringstream processing_stream(response);
    // Setup a new vector for the response arguments.
    response_arguments = std::vector<std::string>();
    // Iterate through the stream using getline delimited at our comms delimiter.
    std::string argument;
    while(std::getline(processing_stream, argument, commsconfig::kDelim[0])) {
        // Add the argument to the response.
        response_arguments.emplace_back(argument);
    }
}

bool Comms::CheckResponse(const std::vector<std::string>& arguments,
        const std::vector<std::string>& response_arguments, int args_to_check) {
    for (int i = 0; i < args_to_check; i++) {
        if (arguments[i] != response_arguments[i])
            return false;
    }
    return true;
}

void Comms::LogError(commsconfig::ErrorConfig error, const std::string& additional_info) {
    LogWrite(std::string("ERROR:") + "\t\t" + commsconfig::ErrorCode[error] + " " + additional_info);
}

void Comms::LogError(const std::string& error_message) {
    LogWrite(std::string("ERROR:") + "\t\t" + error_message);
}

void Comms::LogMessageSend(const std::string &message_sent) {
    LogWrite(std::string("SENT:") + "\t\t" + message_sent);
}

void Comms::LogMessageResponse(const std::string &message_response) {
    LogWrite(std::string("RECEIVED:") + "\t" + message_response);
}

void Comms::LogMessage(const std::string& message) {
    LogWrite(std::string("MESSAGE:") + "\t" + message);
}

void Comms::LogWrite(const std::string& string) {
    // Get the time.
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    // Lock the log mutex.
    std::unique_lock<std::mutex> lock(log_mutex);
    // Write the time and string to the log file.
    log_file_stream << std::put_time(&tm, "%Y%m%d-%H%M%S ") << string;
    // Add a newline character if it wasn't already on the string.
    if (string.back() != '\n') log_file_stream << std::endl;

}

int Comms::GetElapsedMilliseconds(std::chrono::steady_clock::time_point start) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::steady_clock::now()-start).count();
}
