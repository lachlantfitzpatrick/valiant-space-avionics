#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <thread>
#include <fstream>
#include "Comms.h"
#include "stopwatch.h"
#include <QFontDatabase>
#include <QSerialPortInfo>
#include <QSerialPort>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


public slots:
    void update();

private slots:
    void on_connectBtn_clicked();

    void on_disconnectBtn_clicked();

    void on_testBtn_clicked();

    void on_consoleInput_returnPressed();

    void on_autoStartBtn_clicked();

    void on_autoStopBtn_clicked();

    void on_valveHelium_clicked();

    void on_valveFuelVent_clicked();

    void on_valveFuelMain_clicked();

    void on_fillTankBtn_clicked();

    void on_autoStartBtn_2_clicked();

    void on_unlockBtn_clicked();

private:
    Ui::MainWindow *ui;
    Stopwatch* watch;
    Comms comms;
    int numOuts = 0;
    void printResponse(const QString &s);
    void processError(const QString &s);
    void processTimeout(const QString &s);
    int lineNum = 0;
    void printConsole(const QString &s);
    bool checkingConnection = false;
    bool connectSuccess = false;
    QString port = "";

    bool isConnected = false;

    QSerialPort qSerialPort;

    bool valveHeliumOpen = false;
    bool valveFuelVentOpen = false;
    bool valveFuelMainOpen = false;

    bool heliumInLines = false;
    bool fuelInLines = false;

    bool manualLocked = false;

    std::string GenerateTimestampedFilename(const std::string& filename);
};

#endif // MAINWINDOW_H
